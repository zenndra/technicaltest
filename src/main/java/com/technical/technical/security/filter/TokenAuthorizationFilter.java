package com.technical.technical.security.filter;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.constants.TokenConstants;
import com.technical.technical.factory.ServiceFac;
import com.technical.technical.model.token.GrantedAuthorityCustomModel;
import com.technical.technical.model.token.TokenPayloadModel;
import com.technical.technical.security.token.TokenService;
import com.technical.technical.security.token.UserObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;


public class TokenAuthorizationFilter extends BasicAuthenticationFilter implements ApplicationContextAware {

    private static final Logger logger = LoggerFactory.getLogger(TokenAuthorizationFilter.class);

    public static ConcurrentHashMap<String, Jws<Claims>> tokens = new ConcurrentHashMap<>();
    private ApplicationContext ac;

    public TokenAuthorizationFilter(AuthenticationManager authManager, ApplicationContext ac) {
        super(authManager);
        this.ac = ac;
    }


    @Override
    protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain) throws IOException, ServletException {
        HttpSession httpSession = req.getSession();
        try {
            ServiceFac serviceFac = ac.getBean(ServiceFac.class);
            String header = req.getHeader(TokenConstants.CONST_HEADER_KEY_AUTHORIZATION);

            if (header == null || !header.startsWith(TokenConstants.CONST_HEADER_VALUE_BEARER)) {
                chain.doFilter(req, res);

                return;
            } else {
                String strToken = req.getHeader(TokenConstants.CONST_HEADER_KEY_AUTHORIZATION).
                        replaceAll(TokenConstants.CONST_HEADER_VALUE_BEARER, "");

                String[] strTokenSplit = strToken.split("\\.");
                if (strTokenSplit.length != 3) {
                    chain.doFilter(req, res);
                    return;
                }

                String jwtPayload = strTokenSplit[1];
                String body = new String(Base64.getDecoder().decode(jwtPayload));

                ObjectMapper objectMapper = new ObjectMapper();
                UserObjectMapper agentObjectMapper = objectMapper.readValue(body, UserObjectMapper.class);
                String secretKey = serviceFac.getUserService().getSecretKeyUser(agentObjectMapper.getUserId());
                Jws<Claims> jwsClaims = null;

                httpSession.setAttribute(TokenConstants.CLAIM_SECRET_KEY, secretKey);

                try {
                    jwsClaims = TokenService.parseJwsToken(secretKey, strToken);
                } catch (Exception ex) {
                    logger.error(ex.getMessage());
                    res.sendError(403, "ERROR");
                    chain.doFilter(req, res);
                    return;
                }
                Claims claims = jwsClaims.getBody();

                for (String key : claims.keySet()) {
                    String value = claims.get(key).toString();
                    httpSession.setAttribute(key, value);
                }

                List<GrantedAuthorityCustomModel> list = new ArrayList<GrantedAuthorityCustomModel>();
                list.add(new GrantedAuthorityCustomModel("USER"));

                UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken
                        ("PRINCIPAL", null, list);
                SecurityContextHolder.getContext().setAuthentication(authentication);

            }
        } catch (Exception e) {
            logger.error(ErrorConstants.TOKEN_ERROR, e);
        }

        chain.doFilter(req, res);


    }

    @Override
    public void setApplicationContext(ApplicationContext ac) throws BeansException {
        this.ac = ac;
    }

    public ApplicationContext getContext() {
        return ac;
    }
}