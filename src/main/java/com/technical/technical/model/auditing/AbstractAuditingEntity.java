package com.technical.technical.model.auditing;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.Column;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;
import java.time.Instant;

@MappedSuperclass
@JsonIgnoreProperties(ignoreUnknown = true)
@EntityListeners(AuditingEntityListener.class)
public abstract class AbstractAuditingEntity implements Serializable {
    private static final long serialVersionUID = 1L;

    @Column(name = "createdBy", length = 50, updatable = false)
    @JsonIgnore
    private String createdBy;

    @Column(name = "createdAt", nullable = false)
    @JsonIgnore
    private Instant createdAt = Instant.now();

    @Column(name = "updatedBy", length = 50)
    @JsonIgnore
    private String updateBy;

    @Column(name = "updatedAt")
    @JsonIgnore
    private Instant updateAt;

    @Column(name = "deletedBy", length = 50)
    @JsonIgnore
    private String deleteBy;

    @Column(name = "deletedAt")
    @JsonIgnore
    private Instant deleteAt;

    public AbstractAuditingEntity() {
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Instant getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Instant createdAt) {
        this.createdAt = createdAt;
    }

    public String getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(String updateBy) {
        this.updateBy = updateBy;
    }

    public Instant getUpdateAt() {
        return updateAt;
    }

    public void setUpdateAt(Instant updateAt) {
        this.updateAt = updateAt;
    }

    public String getDeleteBy() {
        return deleteBy;
    }

    public void setDeleteBy(String deleteBy) {
        this.deleteBy = deleteBy;
    }

    public Instant getDeleteAt() {
        return deleteAt;
    }

    public void setDeleteAt(Instant deleteAt) {
        this.deleteAt = deleteAt;
    }

    @Override
    public String toString() {
        return "AbstractAuditingEntity{" +
                "createdBy='" + createdBy + '\'' +
                ", createdAt=" + createdAt +
                ", updateBy='" + updateBy + '\'' +
                ", updateAt=" + updateAt +
                ", deleteBy='" + deleteBy + '\'' +
                ", deleteAt=" + deleteAt +
                '}';
    }
}
