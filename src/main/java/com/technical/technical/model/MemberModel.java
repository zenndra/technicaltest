package com.technical.technical.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.technical.technical.constants.TableConstants;
import com.technical.technical.model.auditing.AbstractAuditingEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@JsonIgnoreProperties
@Table(name = TableConstants.TABLE_MEMBER)
public class MemberModel extends AbstractAuditingEntity {

    @Id
    @Column(name = TableConstants.COLUMN_ID)
    private String id;

    @Column(name = TableConstants.COLUMN_NAMA)
    private String nama;

    @Column(name = TableConstants.COLUMN_TANGGAL_LAHIR)
    private LocalDate tanggalLahir;

    @Column(name = TableConstants.COLUMN_NOMOR)
    private String nomor;

    @Column(name = TableConstants.COLUMN_ALAMAT)
    private String alamat;

    @Column(name = TableConstants.COLUMN_EMAIL)
    private String email;

    public MemberModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public LocalDate getTanggalLahir() {
        return tanggalLahir;
    }

    public void setTanggalLahir(LocalDate tanggalLahir) {
        this.tanggalLahir = tanggalLahir;
    }

    public String getNomor() {
        return nomor;
    }

    public void setNomor(String nomor) {
        this.nomor = nomor;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "MemberModel{" +
                "id='" + id + '\'' +
                ", nama='" + nama + '\'' +
                ", tanggalLahir='" + tanggalLahir + '\'' +
                ", nomor='" + nomor + '\'' +
                ", alamat='" + alamat + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
