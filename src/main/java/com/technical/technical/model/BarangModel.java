package com.technical.technical.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.technical.technical.constants.TableConstants;
import com.technical.technical.model.auditing.AbstractAuditingEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@JsonIgnoreProperties
@Table(name = TableConstants.TABLE_BARANG)
public class BarangModel extends AbstractAuditingEntity {

    @Id
    @Column(name = TableConstants.COLUMN_ID)
    private String id;

    @Column(name = TableConstants.COLUMN_KODE)
    private String kode;

    @Column(name = TableConstants.COLUMN_NAMA)
    private String nama;

    @Column(name = TableConstants.COLUMN_JUMLAH)
    private Integer jumlah;

    @Column(name = TableConstants.COLUMN_HARGA)
    private BigDecimal harga;

    public BarangModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public BigDecimal getHarga() {
        return harga;
    }

    public void setHarga(BigDecimal harga) {
        this.harga = harga;
    }

    @Override
    public String toString() {
        return "BarangModel{" +
                "id='" + id + '\'' +
                ", kode='" + kode + '\'' +
                ", nama='" + nama + '\'' +
                ", jumlah=" + jumlah +
                ", harga=" + harga +
                '}';
    }
}
