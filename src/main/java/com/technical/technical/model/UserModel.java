package com.technical.technical.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.technical.technical.constants.TableConstants;
import com.technical.technical.model.auditing.AbstractAuditingEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@JsonIgnoreProperties
@Table(name = TableConstants.TABLE_USER)
public class UserModel extends AbstractAuditingEntity {

    @Id
    @Column(name = TableConstants.COLUMN_ID)
    private String id;

    @Column(name = TableConstants.COLUMN_USERNAME)
    private String username;

    @Column(name = TableConstants.COLUMN_PASSWORD)
    private String password;

    @Column(name = TableConstants.COLUMN_SECRET_KEY)
    private String secretKey;

    public UserModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSecretKey() {
        return secretKey;
    }

    public void setSecretKey(String secretKey) {
        this.secretKey = secretKey;
    }

    @Override
    public String toString() {
        return "UserModel{" +
                "id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", secretKey='" + secretKey + '\'' +
                '}';
    }
}
