package com.technical.technical.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.technical.technical.constants.TableConstants;
import com.technical.technical.model.auditing.AbstractAuditingEntity;

import javax.persistence.*;

@Entity
@JsonIgnoreProperties
@Table(name = TableConstants.TABLE_TRANSAKSI )
public class TransaksiModel extends AbstractAuditingEntity {

    @Id
    @Column(name = TableConstants.COLUMN_ID)
    private String id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = TableConstants.COLUMN_ID_BARANG)
    private BarangModel idBarang;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = TableConstants.COLUMN_ID_MEMBER)
    private MemberModel idMember;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = TableConstants.COLUMN_ID_PENGIRIMAN)
    private PengirimanModel idPengiriman;

    @Column(name = TableConstants.COLUMN_JUMLAH)
    private Integer jumlah;

    public TransaksiModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BarangModel getIdBarang() {
        return idBarang;
    }

    public void setIdBarang(BarangModel idBarang) {
        this.idBarang = idBarang;
    }

    public MemberModel getIdMember() {
        return idMember;
    }

    public void setIdMember(MemberModel idMember) {
        this.idMember = idMember;
    }

    public PengirimanModel getIdPengiriman() {
        return idPengiriman;
    }

    public void setIdPengiriman(PengirimanModel idPengiriman) {
        this.idPengiriman = idPengiriman;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    @Override
    public String toString() {
        return "TransaksiModel{" +
                "id='" + id + '\'' +
                ", idBarang=" + idBarang +
                ", idMember=" + idMember +
                ", idPengiriman=" + idPengiriman +
                ", jumlah=" + jumlah +
                '}';
    }
}
