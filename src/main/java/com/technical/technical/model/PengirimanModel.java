package com.technical.technical.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.technical.technical.constants.TableConstants;
import com.technical.technical.model.auditing.AbstractAuditingEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Entity
@JsonIgnoreProperties
@Table(name = TableConstants.TABLE_PENGIRIMAN)
public class PengirimanModel extends AbstractAuditingEntity {

    @Id
    @Column(name = TableConstants.COLUMN_ID)
    private String id;

    @Column(name = TableConstants.COLUMN_ALAMAT)
    private String alamat;

    @Column(name = TableConstants.COLUMN_PENGIRIM)
    private String pengirim;

    @Column(name = TableConstants.COLUMN_PENERIMA)
    private String penerima;

    @Column(name = TableConstants.COLUMN_WAKTU_KIRIM)
    private Instant waktuKirim;

    @Column(name = TableConstants.COLUMN_WAKTU_TERIMA)
    private Instant waktuTerima;

    public PengirimanModel() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getPengirim() {
        return pengirim;
    }

    public void setPengirim(String pengirim) {
        this.pengirim = pengirim;
    }

    public String getPenerima() {
        return penerima;
    }

    public void setPenerima(String penerima) {
        this.penerima = penerima;
    }

    public Instant getWaktuKirim() {
        return waktuKirim;
    }

    public void setWaktuKirim(Instant waktuKirim) {
        this.waktuKirim = waktuKirim;
    }

    public Instant getWaktuTerima() {
        return waktuTerima;
    }

    public void setWaktuTerima(Instant waktuTerima) {
        this.waktuTerima = waktuTerima;
    }

    @Override
    public String toString() {
        return "PengirimanModel{" +
                "id='" + id + '\'' +
                ", alamat='" + alamat + '\'' +
                ", pengirim='" + pengirim + '\'' +
                ", penerima='" + penerima + '\'' +
                ", waktuKirim=" + waktuKirim +
                ", waktuTerima=" + waktuTerima +
                '}';
    }
}
