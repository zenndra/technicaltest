package com.technical.technical.model.token;

import org.springframework.security.core.GrantedAuthority;

public class GrantedAuthorityCustomModel implements GrantedAuthority {

    private String authority;

    public GrantedAuthorityCustomModel(String authority) {
        this.authority = authority;
    }

    public String getAuthority() {
        return authority;
    }
}
