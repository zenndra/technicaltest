package com.technical.technical.controller;


import com.technical.technical.constants.Constants;
import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.constants.TokenConstants;
import com.technical.technical.dto.MemberDTO;
import com.technical.technical.dto.TransaksiDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.factory.ServiceFac;
import com.technical.technical.service.MemberService;
import com.technical.technical.service.TransaksiService;
import com.technical.technical.util.response.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@RestController
@RequestMapping("/api/transaksi")
public class TransaksiController {

    @Autowired
    ServiceFac serviceFac;

//    @RequestMapping(value = "/view/all", method = RequestMethod.GET)
//    public ResponseEntity<RestResponse<List<MemberDTO>>> viewAllMember(@RequestHeader String Authorization) {
//        RestResponse<List<MemberDTO>> result = new RestResponse<>();
//        try {
//            MemberService memberService = serviceFac.getMemberService();
//            result.setMessage("");
//            result.setSuccess(true);
//            result.setData(memberService.findAllMember(null));
//            HttpStatus httpStatus = HttpStatus.OK;
//            return new ResponseEntity<>(result, httpStatus);
//        } catch (Exception ex) {
//            result.setMessage(ErrorConstants.GENERAL_ERROR);
//            result.setSuccess(false);
//            HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
//            return new ResponseEntity<>(result, httpStatus);
//        }
//    }
//
//    @RequestMapping(value = "/view", method = RequestMethod.GET)
//    public ResponseEntity<RestResponse<MemberDTO>> viewMember(@RequestHeader String Authorization, @RequestParam String idMember) {
//        RestResponse<MemberDTO> result = new RestResponse<>();
//        try {
//            MemberService memberService = serviceFac.getMemberService();
//            result.setMessage("");
//            result.setSuccess(true);
//            result.setData(memberService.findMember(idMember));
//            HttpStatus httpStatus = HttpStatus.OK;
//            return new ResponseEntity<>(result, httpStatus);
//        } catch (Exception ex) {
//            result.setMessage(ErrorConstants.GENERAL_ERROR);
//            result.setSuccess(false);
//            HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
//            return new ResponseEntity<>(result, httpStatus);
//        }
//    }

    @RequestMapping(value = "/input", method = RequestMethod.POST)
    public ResponseEntity<RestResponse<String>> createMember(@RequestHeader String Authorization, @RequestBody TransaksiDTO.InputTransaksiDTO inputTransaksiDTO, HttpServletRequest request) {
        HttpSession session = request.getSession();
        RestResponse<String> result = new RestResponse<>();
        try {
            TransaksiService transaksiService = serviceFac.getTransaksiServiceInput(inputTransaksiDTO);
            String userId = session.getAttribute(TokenConstants.CLAIM_USER_ID).toString();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(transaksiService.inputTransaksi(inputTransaksiDTO, userId));
            HttpStatus httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (CustomException ex) {
            result.setMessage(ex.getMessage());
            result.setSuccess(false);
            HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            result.setSuccess(false);
            HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<>(result, httpStatus);
        }
    }
}
