package com.technical.technical.controller;

import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.BarangDTO;
import com.technical.technical.dto.LoginDTO;
import com.technical.technical.dto.RegisterUserDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.factory.ServiceFac;
import com.technical.technical.service.BarangService;
import com.technical.technical.service.PublicService;
import com.technical.technical.util.response.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.security.PermitAll;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/public")
public class PublicController {

    @Autowired
    ServiceFac serviceFac;

    Logger logger = LoggerFactory.getLogger(PublicController.class);

    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public ResponseEntity<RestResponse<String>> register(@RequestBody RegisterUserDTO registerUserDTO) {
        RestResponse<String> result = new RestResponse<>();
        try {
            PublicService publicService = serviceFac.getPublicServiceRegister(registerUserDTO);
            result.setMessage("");
            result.setSuccess(true);
            result.setData(publicService.register(registerUserDTO));
            HttpStatus httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (CustomException ex) {
            result.setMessage(ex.getMessage());
            result.setSuccess(false);
            HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            result.setSuccess(false);
            HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<>(result, httpStatus);
        }
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<RestResponse<LoginDTO.LoginResponseDTO>> register(@RequestBody LoginDTO.LoginRequestDTO loginRequestDTO) {
        RestResponse<LoginDTO.LoginResponseDTO> result = new RestResponse<>();
        try {
            PublicService publicService = serviceFac.getPublicService();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(publicService.login(loginRequestDTO));
            HttpStatus httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (CustomException ex) {
            result.setMessage(ex.getMessage());
            result.setSuccess(false);
            HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            result.setSuccess(false);
            HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<>(result, httpStatus);
        }
    }

    @RequestMapping(value = "/show/barang/active", method = RequestMethod.GET)
    public ResponseEntity<RestResponse<List<BarangDTO>>> showBarangActive() {
        RestResponse<List<BarangDTO>> result = new RestResponse<>();
        try {
            BarangService barangService = serviceFac.getBarangService();
            result.setMessage("");
            result.setSuccess(true);
            result.setData(barangService.findAllActiveBarang());
            HttpStatus httpStatus = HttpStatus.OK;
            return new ResponseEntity<>(result, httpStatus);
        } catch (Exception ex) {
            result.setMessage(ErrorConstants.GENERAL_ERROR);
            result.setSuccess(false);
            HttpStatus httpStatus = HttpStatus.INTERNAL_SERVER_ERROR;
            return new ResponseEntity<>(result, httpStatus);
        }
    }
}
