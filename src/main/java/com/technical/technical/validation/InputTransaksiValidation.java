package com.technical.technical.validation;

import com.mysql.jdbc.StringUtils;
import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.MemberDTO;
import com.technical.technical.dto.TransaksiDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.model.BarangModel;
import com.technical.technical.repository.BarangRepository;
import com.technical.technical.repository.MemberRepository;
import com.technical.technical.validation.implementation.Specification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class InputTransaksiValidation implements Specification {
    private Logger logger = LoggerFactory.getLogger(InputTransaksiValidation.class);

    private TransaksiDTO.InputTransaksiDTO inputTransaksiDTO;
    private MemberRepository memberRepository;
    private BarangRepository barangRepository;

    public InputTransaksiValidation(TransaksiDTO.InputTransaksiDTO inputTransaksiDTO, MemberRepository memberRepository, BarangRepository barangRepository) {
        this.inputTransaksiDTO = inputTransaksiDTO;
        this.memberRepository = memberRepository;
        this.barangRepository = barangRepository;
    }

    @Override
    public void mandatory() throws CustomException {
        if (StringUtils.isNullOrEmpty(inputTransaksiDTO.getIdBarang())) {
            logger.error(ErrorConstants.NOTHING_TO_TRADE);
            throw new CustomException(ErrorConstants.NOTHING_TO_TRADE);
        }

        if (inputTransaksiDTO.getJumlah() == null || inputTransaksiDTO.getJumlah() == 0) {
            logger.error(ErrorConstants.NOTHING_TO_TRADE);
            throw new CustomException(ErrorConstants.NOTHING_TO_TRADE);
        }

        if (StringUtils.isNullOrEmpty(inputTransaksiDTO.getIdMember()) && StringUtils.isNullOrEmpty(inputTransaksiDTO.getAlamat())) {
            logger.error(ErrorConstants.NOT_MEMBER_AND_EMPTY_ALAMAT);
            throw new CustomException(ErrorConstants.NOT_MEMBER_AND_EMPTY_ALAMAT);
        }

        if(inputTransaksiDTO.getKirim() == null){
            logger.error(ErrorConstants.NEED_TO_KIRIM_OR_NOT);
            throw new CustomException(ErrorConstants.NEED_TO_KIRIM_OR_NOT);
        }
        if(!StringUtils.isNullOrEmpty(inputTransaksiDTO.getIdMember()) && inputTransaksiDTO.getAlamatMember()==null){
            logger.error(ErrorConstants.KIRIM_MEMBER_OR_NOT);
            throw new CustomException(ErrorConstants.KIRIM_MEMBER_OR_NOT);
        }
    }

    @Override
    public void validate() throws CustomException {
        mandatory();
        Optional<BarangModel> barang = barangRepository.findById(inputTransaksiDTO.getIdBarang());
        if (!barang.isPresent()) {
            logger.error(ErrorConstants.BARANG_NOT_FOUND);
            throw new CustomException(ErrorConstants.BARANG_NOT_FOUND);
        }
        if (barang.get().getJumlah() == null || barang.get().getJumlah() == 0) {
            logger.error(ErrorConstants.BARANG_OUT_STOCK);
            throw new CustomException(ErrorConstants.BARANG_OUT_STOCK);
        }

        if (barang.get().getJumlah() < inputTransaksiDTO.getJumlah()) {
            logger.error(ErrorConstants.BARANG_STOCK_NOT_ENOUGH);
            throw new CustomException(ErrorConstants.BARANG_STOCK_NOT_ENOUGH);
        }

        if (!StringUtils.isNullOrEmpty(inputTransaksiDTO.getIdMember())) {
            if (!memberRepository.findById(inputTransaksiDTO.getIdMember()).isPresent()) {
                logger.error(ErrorConstants.MEMBER_NOT_FOUND);
                throw new CustomException(ErrorConstants.MEMBER_NOT_FOUND);
            }
        }

        if(inputTransaksiDTO.getKirim()){

        }
    }
}
