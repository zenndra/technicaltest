package com.technical.technical.validation;

import com.mysql.jdbc.StringUtils;
import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.BarangDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.repository.BarangRepository;
import com.technical.technical.validation.implementation.Specification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BarangCreateValidation implements Specification {
    private Logger logger = LoggerFactory.getLogger(BarangCreateValidation.class);

    private BarangDTO barangDTO;
    private BarangRepository barangRepository;

    public BarangCreateValidation(BarangDTO barangDTO, BarangRepository barangRepository) {
        this.barangDTO = barangDTO;
        this.barangRepository = barangRepository;
    }

    @Override
    public void mandatory() throws CustomException {
        if (StringUtils.isNullOrEmpty(barangDTO.getNama())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "nama"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (barangDTO.getHarga() == null) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "harga"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (barangDTO.getJumlah() == null) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "jumlah"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
    }

    @Override
    public void validate() throws CustomException {
        mandatory();
        if (barangRepository.findByKode(barangDTO.getKode()).isPresent()) {
            logger.error(ErrorConstants.REGISTERED_KODE);
            throw new CustomException(ErrorConstants.REGISTERED_KODE);
        }
    }
}
