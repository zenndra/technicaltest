package com.technical.technical.validation.implementation;


import com.technical.technical.exception.CustomException;

import java.util.List;

public interface Validate {
    void validate() throws CustomException;

    void setValidationManagers(List<Specification> validationmanagers);
}
