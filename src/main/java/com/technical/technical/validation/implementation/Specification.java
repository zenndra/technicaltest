package com.technical.technical.validation.implementation;


import com.technical.technical.exception.CustomException;

public interface Specification {
    void mandatory() throws CustomException;

    void validate() throws CustomException;

    //you can add more validation
}
