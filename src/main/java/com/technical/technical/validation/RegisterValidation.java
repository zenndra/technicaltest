package com.technical.technical.validation;

import com.mysql.jdbc.StringUtils;
import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.RegisterUserDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.repository.UserRepository;
import com.technical.technical.validation.implementation.Specification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RegisterValidation implements Specification {
    private Logger logger = LoggerFactory.getLogger(RegisterValidation.class);

    private RegisterUserDTO registerUserDTO;
    private UserRepository userRepository;

    public RegisterValidation(RegisterUserDTO registerUserDTO, UserRepository userRepository) {
        this.registerUserDTO = registerUserDTO;
        this.userRepository = userRepository;
    }

    @Override
    public void mandatory() throws CustomException {
        if (StringUtils.isNullOrEmpty(registerUserDTO.getUsername())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "username"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (StringUtils.isNullOrEmpty(registerUserDTO.getPassword())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "password"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
    }

    @Override
    public void validate() throws CustomException {
        mandatory();
        if (userRepository.findByUsername(registerUserDTO.getUsername()).isPresent()) {
            logger.error(ErrorConstants.REGISTERED_USERNAME);
            throw new CustomException(ErrorConstants.REGISTERED_USERNAME);
        }
    }
}
