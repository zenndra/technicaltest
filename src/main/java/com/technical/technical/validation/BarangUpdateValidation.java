package com.technical.technical.validation;

import com.mysql.jdbc.StringUtils;
import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.BarangDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.model.BarangModel;
import com.technical.technical.repository.BarangRepository;
import com.technical.technical.validation.implementation.Specification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class BarangUpdateValidation implements Specification {
    private Logger logger = LoggerFactory.getLogger(BarangUpdateValidation.class);

    private BarangDTO barangDTO;
    private BarangRepository barangRepository;

    public BarangUpdateValidation(BarangDTO barangDTO, BarangRepository barangRepository) {
        this.barangDTO = barangDTO;
        this.barangRepository = barangRepository;
    }

    @Override
    public void mandatory() throws CustomException {
        if (StringUtils.isNullOrEmpty(barangDTO.getId())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "id"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (StringUtils.isNullOrEmpty(barangDTO.getNama())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "nama"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (barangDTO.getHarga() == null) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "harga"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (barangDTO.getJumlah() == null) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "jumlah"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
    }

    @Override
    public void validate() throws CustomException {
        mandatory();
        Optional<BarangModel> findBarang = barangRepository.findById(barangDTO.getId());
        if (!findBarang.isPresent()) {
            logger.error(ErrorConstants.BARANG_NOT_FOUND);
            throw new CustomException(ErrorConstants.BARANG_NOT_FOUND);
        }
        if (!findBarang.get().getKode().equals(barangDTO.getKode())) {
            if (barangRepository.findByKode(barangDTO.getKode()).isPresent()) {
                logger.error(ErrorConstants.REGISTERED_KODE);
                throw new CustomException(ErrorConstants.REGISTERED_KODE);
            }
        }
    }
}
