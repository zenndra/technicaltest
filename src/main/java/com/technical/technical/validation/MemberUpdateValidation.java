package com.technical.technical.validation;

import com.mysql.jdbc.StringUtils;
import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.MemberDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.model.MemberModel;
import com.technical.technical.repository.MemberRepository;
import com.technical.technical.validation.implementation.Specification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Optional;

public class MemberUpdateValidation implements Specification {
    private Logger logger = LoggerFactory.getLogger(MemberUpdateValidation.class);

    private MemberDTO memberDTO;
    private MemberRepository memberRepository;

    public MemberUpdateValidation(MemberDTO memberDTO, MemberRepository memberRepository) {
        this.memberDTO = memberDTO;
        this.memberRepository = memberRepository;
    }

    @Override
    public void mandatory() throws CustomException {
        if (StringUtils.isNullOrEmpty(memberDTO.getId())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "id"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (StringUtils.isNullOrEmpty(memberDTO.getNama())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "nama"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (StringUtils.isNullOrEmpty(memberDTO.getNomor())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "nomor"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (StringUtils.isNullOrEmpty(memberDTO.getEmail())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "email"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (StringUtils.isNullOrEmpty(memberDTO.getAlamat())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "alamat"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (memberDTO.getTanggalLahir() == null) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "tanggal lahir"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
    }

    @Override
    public void validate() throws CustomException {
        mandatory();
        Optional<MemberModel> memberModel = memberRepository.findById(memberDTO.getId());
        if (!memberModel.isPresent()) {
            logger.error(ErrorConstants.MEMBER_NOT_FOUND);
            throw new CustomException(ErrorConstants.MEMBER_NOT_FOUND);
        }
        if (!memberModel.get().getNama().equals(memberDTO.getNama())) {
            if (memberRepository.findByNamaAndTanggalLahir(memberDTO.getNama(), memberModel.get().getTanggalLahir()).isPresent()) {
                throw new CustomException(ErrorConstants.REGISTERED_MEMBER);
            }
        }
        if (!memberModel.get().getTanggalLahir().equals(memberDTO.getTanggalLahir())) {
            if (memberRepository.findByNamaAndTanggalLahir(memberModel.get().getNama(), memberDTO.getTanggalLahir()).isPresent()) {
                throw new CustomException(ErrorConstants.REGISTERED_MEMBER);
            }
        }

    }
}
