package com.technical.technical.validation;

import com.mysql.jdbc.StringUtils;
import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.BarangDTO;
import com.technical.technical.dto.MemberDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.repository.BarangRepository;
import com.technical.technical.repository.MemberRepository;
import com.technical.technical.validation.implementation.Specification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MemberCreateValidation implements Specification {
    private Logger logger = LoggerFactory.getLogger(MemberCreateValidation.class);

    private MemberDTO memberDTO;
    private MemberRepository memberRepository;

    public MemberCreateValidation(MemberDTO memberDTO, MemberRepository memberRepository) {
        this.memberDTO = memberDTO;
        this.memberRepository = memberRepository;
    }

    @Override
    public void mandatory() throws CustomException {
        if (StringUtils.isNullOrEmpty(memberDTO.getNama())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "nama"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (StringUtils.isNullOrEmpty(memberDTO.getNomor())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "nomor"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (StringUtils.isNullOrEmpty(memberDTO.getEmail())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "email"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (StringUtils.isNullOrEmpty(memberDTO.getAlamat())) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "alamat"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
        if (memberDTO.getTanggalLahir() == null) {
            logger.error(String.format(ErrorConstants.NULL_PARAMETER, "tanggal lahir"));
            throw new CustomException(ErrorConstants.NULL_VALUE);
        }
    }

    @Override
    public void validate() throws CustomException {
        mandatory();
        if (memberRepository.findByNamaAndTanggalLahir(memberDTO.getNama(), memberDTO.getTanggalLahir()).isPresent()) {
            throw new CustomException(ErrorConstants.REGISTERED_MEMBER);
        }
    }
}
