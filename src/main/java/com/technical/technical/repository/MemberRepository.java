package com.technical.technical.repository;

import com.technical.technical.model.MemberModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.Optional;

public interface MemberRepository extends JpaRepository<MemberModel, String> {

    Optional<MemberModel> findByNamaAndTanggalLahir(String nama, LocalDate tanggalLahir);
}
