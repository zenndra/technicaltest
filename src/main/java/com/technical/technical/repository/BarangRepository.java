package com.technical.technical.repository;

import com.technical.technical.model.BarangModel;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface BarangRepository extends JpaRepository<BarangModel, String> {
    List<BarangModel> findAllByDeleteAtIsNull();

    Optional<BarangModel> findByKode(String kode);
}
