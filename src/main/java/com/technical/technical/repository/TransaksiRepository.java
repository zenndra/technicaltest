package com.technical.technical.repository;

import com.technical.technical.model.TransaksiModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TransaksiRepository extends JpaRepository<TransaksiModel, String> {
}
