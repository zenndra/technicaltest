package com.technical.technical.repository;

import com.technical.technical.model.PengirimanModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PengirimanRepository extends JpaRepository<PengirimanModel, String> {
}
