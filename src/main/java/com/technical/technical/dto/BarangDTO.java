package com.technical.technical.dto;

import com.technical.technical.model.BarangModel;

import java.math.BigDecimal;

public class BarangDTO {

    private String id;
    private String kode;
    private String nama;
    private Integer jumlah;
    private BigDecimal harga;

    public BarangDTO() {
    }

    public BarangDTO(BarangModel barangModel) {
        this.id = barangModel.getId();
        this.kode = barangModel.getKode();
        this.nama = barangModel.getNama();
        this.jumlah = barangModel.getJumlah();
        this.harga = barangModel.getHarga();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public Integer getJumlah() {
        return jumlah;
    }

    public void setJumlah(Integer jumlah) {
        this.jumlah = jumlah;
    }

    public BigDecimal getHarga() {
        return harga;
    }

    public void setHarga(BigDecimal harga) {
        this.harga = harga;
    }
}
