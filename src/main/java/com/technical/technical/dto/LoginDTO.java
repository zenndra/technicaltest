package com.technical.technical.dto;

import javax.validation.constraints.NotNull;

public class LoginDTO {
    public static class LoginRequestDTO {

        @NotNull
        private String username;
        @NotNull
        private String password;

        public LoginRequestDTO() {
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        @Override
        public String toString() {
            return "LoginRequestDTO{" +
                    "username='" + username + '\'' +
                    ", password='" + password + '\'' +
                    '}';
        }
    }

    public static class LoginResponseDTO {
        private String token;

        public LoginResponseDTO() {
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        @Override
        public String toString() {
            return "LoginResponseDTO{" +
                    "token='" + token + '\'' +
                    '}';
        }
    }
}
