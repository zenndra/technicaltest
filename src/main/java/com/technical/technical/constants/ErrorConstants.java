package com.technical.technical.constants;

public class ErrorConstants {
    public static final String GENERAL_ERROR = "Something went wrong";
    public static final String TOKEN_ERROR = "Token Error";

    public static final String NULL_VALUE = "Some parameter is null";
    public static final String NULL_PARAMETER = "%s is null";

    public static final String REGISTERED_USERNAME = "Username already registered";
    public static final String REGISTERED_KODE = "Kode already registered";
    public static final String REGISTERED_MEMBER = "User already registered";
    public static final String BARANG_NOT_FOUND = "Barang Not Found";
    public static final String MEMBER_NOT_FOUND = "Member Not Found";

    public static final String BARANG_OUT_STOCK = "Barang Out Of Stock";
    public static final String BARANG_STOCK_NOT_ENOUGH = "Barang Stock Not Ready";

    public static final String NOT_MEMBER_AND_EMPTY_ALAMAT = "If not member then alamat cannot be blank";
    public static final String NOTHING_TO_TRADE = "There is no Barang to be trade";
    public static final String NEED_TO_KIRIM_OR_NOT = "Please Choose Kirim";
    public static final String KIRIM_MEMBER_OR_NOT = "Please Choose Alamat Member";
}
