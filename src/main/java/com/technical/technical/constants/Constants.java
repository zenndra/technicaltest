package com.technical.technical.constants;

public class Constants {

    public static final String ACTION_INSERT = "INSERT";
    public static final String ACTION_UPDATE = "UPDATE";
    public static final String ACTION_DELETE = "DELETE";

    public static final String TAKE_IT_SELF = "Dibawa sendiri";
    public static final String COURIER = "Courier";
}
