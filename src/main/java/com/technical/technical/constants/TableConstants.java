package com.technical.technical.constants;

public class TableConstants {

    public static final String TABLE_USER = "user";
    public static final String TABLE_BARANG = "barang";
    public static final String TABLE_MEMBER = "member";
    public static final String TABLE_PENGIRIMAN = "pengiriman";
    public static final String TABLE_TRANSAKSI = "transaksi";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_PASSWORD = "password";
    public static final String COLUMN_SECRET_KEY = "secret_key";
    public static final String COLUMN_KODE = "kode";
    public static final String COLUMN_NAMA = "nama";
    public static final String COLUMN_HARGA = "harga";
    public static final String COLUMN_JUMLAH = "jumlah";
    public static final String COLUMN_TANGGAL_LAHIR = "tanggal_lahir";
    public static final String COLUMN_NOMOR = "nomor";
    public static final String COLUMN_ALAMAT = "alamat";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_PENGIRIM = "pengirim";
    public static final String COLUMN_PENERIMA = "penerima";
    public static final String COLUMN_WAKTU_KIRIM = "waktu_kirim";
    public static final String COLUMN_WAKTU_TERIMA = "waktu_terima";
    public static final String COLUMN_ID_BARANG = "id_barang";
    public static final String COLUMN_ID_MEMBER = "id_member";
    public static final String COLUMN_ID_PENGIRIMAN = "id_pengiriman";
}
