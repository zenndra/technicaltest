package com.technical.technical.service;

import com.technical.technical.constants.TokenConstants;
import com.technical.technical.dto.LoginDTO;
import com.technical.technical.dto.RegisterUserDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.factory.RepoFac;
import com.technical.technical.model.UserModel;
import com.technical.technical.repository.UserRepository;
import com.technical.technical.security.token.TokenService;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import com.technical.technical.validation.implementation.Validate;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.*;

@Service
public class PublicService implements Validate {

    List<Specification> validationmanagers;

    @Autowired
    private RepoFac repoFac;

    public String register(RegisterUserDTO registerUserDTO) throws CustomException {
        validate();
        UserRepository userRepository = repoFac.getUserRepository();
        UserModel userModel = new UserModel();
        userModel.setId(Util.generateRandomUUID());
        userModel.setUsername(registerUserDTO.getUsername());
        userModel.setPassword(Util.getBCryptEncoder().encode(registerUserDTO.getPassword()));
        userModel.setSecretKey(Base64.getEncoder().encodeToString(Util.generateRandomUUID().getBytes()));
        userRepository.save(userModel);
        return "Register success";
    }

    public LoginDTO.LoginResponseDTO login(LoginDTO.LoginRequestDTO loginRequestDTO) throws CustomException, InvalidKeySpecException, NoSuchAlgorithmException {
        LoginDTO.LoginResponseDTO result = new LoginDTO.LoginResponseDTO();
        UserRepository userRepository = repoFac.getUserRepository();
        Optional<UserModel> userModel = userRepository.findByUsername(loginRequestDTO.getUsername());
        if (userModel.isPresent()) {
            if (Util.getBCryptEncoder().matches(loginRequestDTO.getPassword(), userModel.get().getPassword())) {
                Date now = Util.getNow();
                Date tokenExpired = Util.addTimeUnitToDate(now, Calendar.MINUTE, 15);
                SignatureAlgorithm alg = SignatureAlgorithm.HS256;
                Map<String, String> claims = new HashMap<>();
                claims.put(TokenConstants.CLAIM_USER_ID, userModel.get().getId());
                result.setToken(TokenService.createJwtToken(TokenConstants.TOKEN_ISSUER, tokenExpired, now, alg, userModel.get().getSecretKey(), claims));
            } else {
                throw new CustomException("Password Invalid");
            }
        } else {
            throw new CustomException("Username not found");
        }

        return result;
    }

    public List<Specification> getValidationmanagers() {
        return validationmanagers;
    }

    public void setValidationmanagers(List<Specification> validationmanagers) {
        this.validationmanagers = validationmanagers;
    }

    @Override
    public void validate() throws CustomException {
        for (Specification specification : validationmanagers) {
            specification.validate();
        }
    }

    @Override
    public void setValidationManagers(List<Specification> validationmanagers) {
        this.validationmanagers = validationmanagers;
    }
}
