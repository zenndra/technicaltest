package com.technical.technical.service;

import com.technical.technical.factory.RepoFac;
import com.technical.technical.model.UserModel;
import com.technical.technical.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    RepoFac repoFac;

    public String getSecretKeyUser(String userId) {
        UserRepository userRepository = repoFac.getUserRepository();
        Optional<UserModel> userModel = userRepository.findById(userId);
        if (userModel.isPresent()) {
            return userModel.get().getSecretKey();
        }
        return null;
    }
}
