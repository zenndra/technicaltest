package com.technical.technical.service;

import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.BarangDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.factory.RepoFac;
import com.technical.technical.model.BarangModel;
import com.technical.technical.repository.BarangRepository;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import com.technical.technical.validation.implementation.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class BarangService implements Validate {

    List<Specification> validationmanagers;

    @Autowired
    private RepoFac repoFac;

    public List<BarangDTO> findAllActiveBarang() {
        BarangRepository barangRepository = repoFac.getBarangRepository();
        List<BarangModel> activeBarang = barangRepository.findAllByDeleteAtIsNull();
        return activeBarang.stream().map(BarangDTO::new).collect(Collectors.toList());
    }

    public BarangDTO viewBarang(String idBarang) {
        BarangRepository barangRepository = repoFac.getBarangRepository();
        Optional<BarangModel> findBarang = barangRepository.findById(idBarang);
        BarangDTO result = new BarangDTO();
        if (findBarang.isPresent()) {
            result = new BarangDTO(findBarang.get());
        }
        return result;
    }

    public String insertBarang(BarangDTO barang, String userId) throws CustomException {
        validate();
        BarangRepository barangRepository = repoFac.getBarangRepository();
        BarangModel barangModel = new BarangModel();
        barangModel.setId(Util.generateRandomUUID());
        barangModel.setKode(barang.getKode());
        barangModel.setNama(barang.getNama());
        barangModel.setJumlah(barang.getJumlah());
        barangModel.setHarga(barang.getHarga());
        barangModel.setCreatedBy(userId);
        barangRepository.save(barangModel);
        return "Insert Barang Success";
    }

    public String updateBarang(BarangDTO barang, String userId) throws CustomException {
        validate();
        BarangRepository barangRepository = repoFac.getBarangRepository();
        BarangModel editBarang = barangRepository.getOne(barang.getId());
        editBarang.setKode(barang.getKode());
        editBarang.setJumlah(barang.getJumlah());
        editBarang.setNama(barang.getNama());
        editBarang.setHarga(barang.getHarga());
        editBarang.setUpdateAt(Instant.now());
        editBarang.setUpdateBy(userId);
        barangRepository.save(editBarang);
        return "Edit Barang Success";
    }

    public String deleteBarang(String idBarang, String userId) throws CustomException {
        BarangRepository barangRepository = repoFac.getBarangRepository();
        Optional<BarangModel> findBarang = barangRepository.findById(idBarang);
        if (findBarang.isPresent()) {
            BarangModel deleteBarang = findBarang.get();
            deleteBarang.setDeleteAt(Instant.now());
            deleteBarang.setDeleteBy(userId);
            barangRepository.save(deleteBarang);
            return "Delete Barang Success";
        } else {
            throw new CustomException(ErrorConstants.BARANG_NOT_FOUND);
        }
    }

    public void beliBarang(String idBarang, Integer jumlahBeli) {
        BarangRepository barangRepository = repoFac.getBarangRepository();
        BarangModel barang = barangRepository.getOne(idBarang);
        barang.setJumlah(barang.getJumlah() - jumlahBeli);
        barangRepository.save(barang);
    }

    @Override
    public void validate() throws CustomException {
        for (Specification specification : validationmanagers) {
            specification.validate();
        }
    }

    @Override
    public void setValidationManagers(List<Specification> validationmanagers) {
        this.validationmanagers = validationmanagers;
    }
}
