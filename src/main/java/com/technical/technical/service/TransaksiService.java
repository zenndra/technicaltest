package com.technical.technical.service;

import com.mysql.jdbc.StringUtils;
import com.technical.technical.constants.Constants;
import com.technical.technical.dto.TransaksiDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.factory.RepoFac;
import com.technical.technical.factory.ServiceFac;
import com.technical.technical.model.MemberModel;
import com.technical.technical.model.PengirimanModel;
import com.technical.technical.model.TransaksiModel;
import com.technical.technical.repository.BarangRepository;
import com.technical.technical.repository.MemberRepository;
import com.technical.technical.repository.PengirimanRepository;
import com.technical.technical.repository.TransaksiRepository;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import com.technical.technical.validation.implementation.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.List;

@Service
public class TransaksiService implements Validate {

    List<Specification> validationmanagers;

    @Autowired
    private RepoFac repoFac;

    @Autowired
    private ServiceFac serviceFac;

    @Transactional
    public String inputTransaksi(TransaksiDTO.InputTransaksiDTO inputTransaksi, String userId) throws CustomException {
        validate();
        TransaksiRepository transaksiRepository = repoFac.getTransaksiRepository();
        BarangRepository barangRepository = repoFac.getBarangRepository();
        MemberRepository memberRepository = repoFac.getMemberRepository();
        PengirimanRepository pengirimanRepository = repoFac.getPengirimanRepository();
        BarangService barangService = serviceFac.getBarangService();

        TransaksiModel dataTransaksi = new TransaksiModel();
        dataTransaksi.setId(Util.generateRandomUUID());
        dataTransaksi.setIdBarang(barangRepository.getOne(inputTransaksi.getIdBarang()));
        dataTransaksi.setJumlah(inputTransaksi.getJumlah());
        dataTransaksi.setCreatedBy(userId);

        barangService.beliBarang(inputTransaksi.getIdBarang(), inputTransaksi.getJumlah());
        String alamatKirim = null;
        String pengirim = null;
        String penerima = inputTransaksi.getNamaPembeli();
        MemberModel memberModel = null;
        if (!StringUtils.isNullOrEmpty(inputTransaksi.getIdMember())) {
            memberModel = memberRepository.getOne(inputTransaksi.getIdMember());
            dataTransaksi.setIdMember(memberModel);
        }
        if (inputTransaksi.getKirim()) {
            if (inputTransaksi.getAlamatMember()) {
                if (memberModel != null) {
                    alamatKirim = memberModel.getAlamat();
                    penerima = memberModel.getNama();
                }
            } else {
                alamatKirim = inputTransaksi.getAlamat();
            }
            pengirim = Constants.COURIER;
        } else {
            pengirim = Constants.TAKE_IT_SELF;
            penerima = Constants.TAKE_IT_SELF;
            alamatKirim = Constants.TAKE_IT_SELF;
        }
        String idPengiriman = inputDataPengiriman(alamatKirim, pengirim, penerima, inputTransaksi.getKirim(), userId);
        dataTransaksi.setIdPengiriman(pengirimanRepository.getOne(idPengiriman));

        transaksiRepository.save(dataTransaksi);
        return "Transaksi Success";
    }

    private String inputDataPengiriman(String alamat, String pengirim, String penerima, Boolean kirim, String userId) {
        PengirimanRepository pengirimanRepository = repoFac.getPengirimanRepository();
        PengirimanModel pengirimanModel = new PengirimanModel();
        pengirimanModel.setId(Util.generateRandomUUID());
        if (!kirim) {
            pengirimanModel.setWaktuTerima(Instant.now());
        }
        pengirimanModel.setPenerima(penerima);
        pengirimanModel.setPengirim(pengirim);
        pengirimanModel.setAlamat(alamat);
        pengirimanModel.setCreatedBy(userId);
        pengirimanRepository.save(pengirimanModel);
        return pengirimanModel.getId();
    }

    @Override
    public void validate() throws CustomException {
        for (Specification specification : validationmanagers) {
            specification.validate();
        }
    }

    @Override
    public void setValidationManagers(List<Specification> validationmanagers) {
        this.validationmanagers = validationmanagers;
    }
}
