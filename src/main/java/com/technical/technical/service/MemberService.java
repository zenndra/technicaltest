package com.technical.technical.service;

import com.technical.technical.constants.ErrorConstants;
import com.technical.technical.dto.MemberDTO;
import com.technical.technical.exception.CustomException;
import com.technical.technical.factory.RepoFac;
import com.technical.technical.model.MemberModel;
import com.technical.technical.repository.MemberRepository;
import com.technical.technical.util.Util;
import com.technical.technical.validation.implementation.Specification;
import com.technical.technical.validation.implementation.Validate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MemberService implements Validate {

    List<Specification> validationmanagers;

    @Autowired
    private RepoFac repoFac;

    public List<MemberDTO> findAllMember(Boolean isActive) {
        MemberRepository memberRepository = repoFac.getMemberRepository();
        List<MemberModel> memberList = memberRepository.findAll();
        if (isActive != null && isActive) {
            memberList = memberList.stream().filter(memberModel -> memberModel.getDeleteAt() == null).collect(Collectors.toList());
        }
        return memberList.stream().map(MemberDTO::new).collect(Collectors.toList());
    }

    public MemberDTO findMember(String idMember) {
        MemberDTO result = null;
        MemberRepository memberRepository = repoFac.getMemberRepository();
        MemberModel member = memberRepository.findById(idMember).orElse(null);
        if (member != null) {
            result = new MemberDTO(member);
        }
        return result;
    }

    public String insertMember(MemberDTO member, String userId) throws CustomException {
        validate();
        MemberRepository memberRepository = repoFac.getMemberRepository();
        MemberModel memberModel = new MemberModel();
        memberModel.setId(Util.generateRandomUUID());
        memberModel.setAlamat(member.getAlamat());
        memberModel.setNama(member.getNama());
        memberModel.setEmail(member.getEmail());
        memberModel.setNomor(member.getNomor());
        memberModel.setTanggalLahir(member.getTanggalLahir());
        memberModel.setCreatedBy(userId);
        memberRepository.save(memberModel);
        return "Create Member Success";
    }

    public String updateMember(MemberDTO member, String userId) throws CustomException {
        validate();
        MemberRepository memberRepository = repoFac.getMemberRepository();
        MemberModel editMember = memberRepository.getOne(member.getId());
        editMember.setAlamat(member.getAlamat());
        editMember.setNama(member.getNama());
        editMember.setEmail(member.getEmail());
        editMember.setNomor(member.getNomor());
        editMember.setTanggalLahir(member.getTanggalLahir());
        editMember.setCreatedBy(userId);
        editMember.setUpdateAt(Instant.now());
        editMember.setUpdateBy(userId);
        memberRepository.save(editMember);
        return "Edit Member Success";
    }

    public String deleteMember(String idMember, String userId) throws CustomException {
        MemberRepository memberRepository = repoFac.getMemberRepository();
        Optional<MemberModel> findBarang = memberRepository.findById(idMember);
        if (findBarang.isPresent()) {
            MemberModel deleteMember = findBarang.get();
            deleteMember.setDeleteAt(Instant.now());
            deleteMember.setDeleteBy(userId);
            memberRepository.save(deleteMember);
            return "Delete Member Success";
        } else {
            throw new CustomException(ErrorConstants.BARANG_NOT_FOUND);
        }
    }

    @Override
    public void validate() throws CustomException {
        for (Specification specification : validationmanagers) {
            specification.validate();
        }
    }

    @Override
    public void setValidationManagers(List<Specification> validationmanagers) {
        this.validationmanagers = validationmanagers;
    }
}
