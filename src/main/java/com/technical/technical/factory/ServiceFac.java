package com.technical.technical.factory;

import com.technical.technical.constants.Constants;
import com.technical.technical.dto.BarangDTO;
import com.technical.technical.dto.MemberDTO;
import com.technical.technical.dto.RegisterUserDTO;
import com.technical.technical.dto.TransaksiDTO;
import com.technical.technical.service.*;
import com.technical.technical.validation.*;
import com.technical.technical.validation.implementation.Specification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceFac {

    @Autowired
    RepoFac repoFac;

    @Autowired
    PublicService publicService;

    @Autowired
    UserService userService;

    @Autowired
    BarangService barangService;

    @Autowired
    MemberService memberService;

    @Autowired
    TransaksiService transaksiService;

    public PublicService getPublicService() {
        return publicService;
    }

    public PublicService getPublicServiceRegister(RegisterUserDTO registerUserDTO) {
        List<Specification> specificationList = new ArrayList<>();
        RegisterValidation registerValidation = new RegisterValidation(registerUserDTO, repoFac.getUserRepository());
        specificationList.add(registerValidation);
        publicService.setValidationManagers(specificationList);
        return publicService;
    }

    public UserService getUserService() {
        return userService;
    }

    public BarangService getBarangService() {
        return barangService;
    }

    public BarangService getBarangServiceWithAction(String action, BarangDTO barangDTO) {
        List<Specification> specificationList = new ArrayList<>();
        if (action.equals(Constants.ACTION_INSERT)) {
            BarangCreateValidation barangCreateValidation = new BarangCreateValidation(barangDTO, repoFac.getBarangRepository());
            specificationList.add(barangCreateValidation);
        } else if (action.equals(Constants.ACTION_UPDATE)) {
            BarangUpdateValidation barangUpdateValidation = new BarangUpdateValidation(barangDTO, repoFac.getBarangRepository());
            specificationList.add(barangUpdateValidation);
        }
        barangService.setValidationManagers(specificationList);
        return barangService;
    }

    public MemberService getMemberService(String action, MemberDTO memberDTO) {
        List<Specification> specificationList = new ArrayList<>();
        if (action.equals(Constants.ACTION_INSERT)) {
            MemberCreateValidation memberCreateValidation = new MemberCreateValidation(memberDTO, repoFac.getMemberRepository());
            specificationList.add(memberCreateValidation);
        } else if (action.equals(Constants.ACTION_UPDATE)) {
            MemberUpdateValidation memberUpdateValidation = new MemberUpdateValidation(memberDTO, repoFac.getMemberRepository());
            specificationList.add(memberUpdateValidation);
        }
        memberService.setValidationManagers(specificationList);
        return memberService;
    }

    public MemberService getMemberService() {
        return memberService;
    }

    public TransaksiService getTransaksiService() {
        return transaksiService;
    }
    public TransaksiService getTransaksiServiceInput(TransaksiDTO.InputTransaksiDTO inputTransaksiDTO) {
        List<Specification> specificationList = new ArrayList<>();
        InputTransaksiValidation inputTransaksiValidation = new InputTransaksiValidation(inputTransaksiDTO, repoFac.getMemberRepository(), repoFac.getBarangRepository());
        specificationList.add(inputTransaksiValidation);
        transaksiService.setValidationManagers(specificationList);
        return transaksiService;
    }
}
