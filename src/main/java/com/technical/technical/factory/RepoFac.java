package com.technical.technical.factory;

import com.technical.technical.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class RepoFac {

    @Autowired
    UserRepository userRepository;

    @Autowired
    BarangRepository barangRepository;

    @Autowired
    MemberRepository memberRepository;

    @Autowired
    PengirimanRepository pengirimanRepository;

    @Autowired
    TransaksiRepository transaksiRepository;

    public UserRepository getUserRepository() {
        return userRepository;
    }

    public BarangRepository getBarangRepository() {
        return barangRepository;
    }

    public MemberRepository getMemberRepository() {
        return memberRepository;
    }

    public PengirimanRepository getPengirimanRepository() {
        return pengirimanRepository;
    }

    public TransaksiRepository getTransaksiRepository() {
        return transaksiRepository;
    }
}
